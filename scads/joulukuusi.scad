$fn=60;
height=150;
iterations=24;
angle=11.5;
incr=0.7;
needle=[80,0.6,0.4];
full=360-(sqrt(5)+1)/2*angle;
a=0.3; b=0.5; c=0.95; d=4;

function tree(p) = 1-0.8*pow(p,2);
function weight(p) = p<a?b*p:a*b+(p-a)*((1-a*b)/(1-a));
function weight2(p) = p<c?weight(p):weight(c)+d*(p-c);
function weight3(p) = weight2(p)/weight2(1);

for(j=[0:iterations]) {    
    for(i=[0:height/incr]){
        h=weight3(i*incr/height)*height;
        translate([0,0,h])
        rotate(i*angle+j/iterations*full,[0,0,1])
            cube([needle[0]*tree(h/height),needle[1],needle[2]]);
    }
}

base=20;
baseD=18;

translate([0,0,-base])
    linear_extrude(height+base, scale=0.1){
        circle(d=baseD);
    }

translate([0,0,-base-3])
    linear_extrude(3, scale=0.7){
        circle(d=baseD/0.7);
    }