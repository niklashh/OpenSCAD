$fn=60;
height=70;
iterations=16;
angle=11.5;
incr=0.5;
needle=[50,0.5,0.3];
full=360-(sqrt(5)+1)/2*angle;
a=0.3; b=0.5;

function tree(p) = 1-0.6*pow(p,2);
function weight(p) = p<a?b*p:a*b+(p-a)*((1-a*b)/(1-a));

for(j=[0:iterations]) {    
    for(i=[0:height/incr]){
        h=weight(i*incr/height)*height;
        translate([0,0,h])
        rotate(i*angle+j/iterations*full,[0,0,1])
            cube([needle[0]*tree(h/height),needle[1],needle[2]]);
    }
}

base=5;
baseD=10;

translate([0,0,-base])
    linear_extrude(height+base, scale=0.5){
        circle(d=baseD);
    }

translate([0,0,-base-3])
    linear_extrude(3, scale=0.7){
        circle(d=baseD/0.7);
    }